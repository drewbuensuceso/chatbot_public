#ui automator test for whatsapp messaging
import uiautomator2 as u2
import logging


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-8s %(message)s')
log = logging.getLogger('whatsapp.bot')

def send_message():
    #test connection to device
    device_id = '7b0c54dd'
    recipient = 'TEST_RECIPIENT'
    msg = 'TEST_MESSAGE'
    target_app = ('com.whatsapp')

    d = u2.connect('7b0c54dd')
    log.info(f'ATX is connected to Device: {device_id}')

    if(not d.info.get('screenOn')):
        log.info(d.info.get('screenOn'))
        d.unlock()
        log.info('screen unlocked')

    if target_app in d.app_list_running(): #checks if app is running and closes it to go back to default state
        d.app_stop(target_app)
        d.implicitly_wait(5.0)
        log.info('stopping current app state')
        
    d.implicitly_wait(5)
    d.app_start(target_app) #opens whatsapp
    d.xpath('//*[@resource-id="com.whatsapp:id/fab"]').click() #new message button
    d.xpath('//*[@resource-id="com.whatsapp:id/menuitem_search"]').click() #search bar
    d.send_keys(recipient, clear=True)
    first_result = d.xpath('//*[@resource-id="android:id/list"]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]')
    first_result.click()
    d(text="Type a message", className='android.widget.EditText').click()
    d.send_keys(msg, clear=True)
    log.info('sending message')
    d.implicitly_wait(5.0)
    d.xpath('//*[@resource-id="com.whatsapp:id/send_container"]').click()
    log.info('sent message sucessfully')
    d.app_stop('com.whatsapp')
    log.info('turning off screen')
    d.screen_off