# chatbot_public
automated messaging using ATX and 3rd party apps

#install adb services
brew install cask --android-platform-tools

#install uiautomator2
pip install --upgrade --pre uiautomator2

#install python packages
pip install -r requirements.txt

#run server
python wsgi.py
