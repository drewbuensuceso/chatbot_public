from flask import Flask
import facebook_bot, whatsapp_bot, shopee_bot

app = Flask(__name__)

@app.route("/facebook")
def send_facebook():
    return facebook_bot.send_message()

@app.route("/whatsapp")
def send_whatsapp():
    return whatsapp_bot.send_message()

@app.route("/shopee/quick_buy")
def buy_item():
    return shopee_bot.quick_buy('HM auto side mirror for click')
@app.route('/shopee/quick_checkout')
def auto_checkout():
    return shopee_bot.quick_checkout()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5023', debug=True)