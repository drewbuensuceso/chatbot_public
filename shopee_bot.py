#automated purchasing bot for shopee app
import uiautomator2 as u2
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-8s %(message)s')
log = logging.getLogger('shopee.bot')
d = u2.connect('7b0c54dd')

def get_app_ready(): #initializes app to default state and if ads pop up, closes the ad
    d.app_start(package_name = 'com.shopee.ph', activity ='com.shopee.app.ui.home.HomeActivity_', stop=True)
    d.wait_activity('com.shopee.app.ui.home.HomeActivity_', timeout=5)
    d.xpath('//androidx.viewpager.widget.ViewPager/\
        android.widget.LinearLayout[1]/\
            android.widget.FrameLayout[1]/\
                android.widget.FrameLayout[1]/\
                    android.view.ViewGroup[6]').wait(3)
    d.xpath('//androidx.viewpager.widget.ViewPager/\
        android.widget.LinearLayout[1]/\
            android.widget.FrameLayout[1]/\
                android.widget.FrameLayout[1]/\
                    android.view.ViewGroup[6]').click_exists(timeout=3)

def search_item(item): #types the item in the search bar and press enter
    d.xpath('//androidx.viewpager.widget.ViewPager/\
        android.widget.LinearLayout[1]/\
            android.widget.FrameLayout[1]/\
                android.widget.FrameLayout[1]/\
                    android.view.ViewGroup[3]').child('android.widget.TextView').click()
    d.send_keys(str(item),clear=True)
    d.press('enter')

def select_first(): ## selects the first search result
    d.xpath('//androidx.recyclerview.widget.RecyclerView/\
        android.view.ViewGroup[3]/android.view.ViewGroup[1]/\
            android.view.ViewGroup[1]/android.view.ViewGroup[1]/\
                android.view.ViewGroup[2]/android.view.ViewGroup[1]/\
                    android.widget.ImageView[2]').click()
def open_cart(): #opens the current cart contents
    d.xpath('//androidx.viewpager.widget.ViewPager/\
        android.widget.LinearLayout[1]/\
            android.widget.FrameLayout[1]/\
                android.widget.FrameLayout[1]/\
                    android.view.ViewGroup[4]/\
                        android.view.ViewGroup[1]/\
                            android.view.ViewGroup[1]/\
                                android.widget.ImageView[1]').click()
def cart_select_first(): #selects the first item inside the cart
    d.xpath('//android.widget.ScrollView/\
        android.view.ViewGroup[1]/\
            android.view.ViewGroup[1]/\
                android.view.ViewGroup[1]/\
                    android.view.ViewGroup[1]/\
                        android.view.ViewGroup[1]').click()

def buy_now(): #presses through shopee's buy now button
    d(textContains='Buy Now').wait(5)
    d(textContains='Buy Now').click()
    d.implicitly_wait(5)
    d(textContains='Buy Now').wait(5)
    d(textContains='Buy Now').click()

def selection_checkout(): #checkout an item
    d(textContains='Check Out').wait(5)
    d(textContains='Check Out').click()

#for adding new address
def input_new_address(fname='Ray Andrew Buensuceso',mobile='09999389040', \
                        staddrr='Block 8 Lot 23 Phase 3 Greenridge Executive Village'):
    d(textContains='Address').click()
    d(textContains='new address').click()
    d.xpath('@com.shopee.ph:id/buttonDefaultNegative').click()
    d(textContains='Full Name').right(className='android.widget.EditText').set_text(fname)
    d.xpath('@input-phone-number').set_text(mobile)
    d.xpath('@set_state_add_address_page').click()
    d(textContains='South Luzon').click() #will be more editable upon revision
    d.xpath('@set_city_add_address_page').click()
    d(textContains='Rizal').click()
    d.xpath('@set_district_add_address_page').click()
    d(textContains='Binangonan').click()
    d.xpath('@set_town_add_address_page').click()
    d(textContains='Bilibiran').click()
    d(textContains='Postal Code').right(className='android.widget.EditText').set_text('1940')
    d(textContains='Detailed Address').down(className='android.widget.EditText').set_text(staddrr)
    d.xpath('@btn-next').click()

def select_newest_address(): #selects the newest address in the addresses page upon checkout
    d(className='android.view.ViewGroup', index=0).child(className='android.view.ViewGroup', index=2).click()

def quick_buy(item): #buys the first item that a search in the app provides and uses a new address
    get_app_ready()
    search_item(item)
    select_first() #selects first result of search
    buy_now()
    selection_checkout()
    input_new_address() #address input
    select_newest_address()
    #d(textContains='Place Order').click() #pushes through with the final purchase step
def quick_checkout():
    get_app_ready()
    open_cart() #opens the current cart from app homepage
    cart_select_first() #selects first item of cart
    selection_checkout()
    input_new_address()
    select_newest_address()
    #d(textContains='Place Order').click() #pushes through with the final purchase step